/**
 * Class: CS-310
 * Southern New Hampshire Universality
 * Date : 2019-04-07
 */

package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;
/**
 * This class add Salif Keita as an artist in the music artist package
 * @author Bourama Mangara
 * @version 1.0
 *
 */
public class SalifKeita {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public SalifKeita() {
    }
    
    public ArrayList<Song> getSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                          //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Tonton", "Salif Keita");             //Create a song
         Song track2 = new Song("Diawara Fa", "Salif Keita");        //Create another song
         this.albumTracks.add(track1);                                   //Add the first song to song list for the Beatles
         this.albumTracks.add(track2);                                  //Add the second song to song list for the Beatles 
         return albumTracks;                                           //Return the songs for the Beatles in the form of an ArrayList
    }
}
