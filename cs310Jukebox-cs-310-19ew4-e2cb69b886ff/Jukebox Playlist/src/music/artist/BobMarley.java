/**
 * Class: CS-310
 * Southern New Hampshire Universality
 * Date : 2019-04-07
 */

package music.artist;

import java.util.ArrayList;
import snhu.jukebox.playlist.Song;
/**
 * This class add Bob Marley as an artist in the music artist package
 * @author Bourama Mangara
 * @version 1.0
 *
 */
public class BobMarley {
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BobMarley() {
    }
    
    public ArrayList<Song> getSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Redemption Song ", "Bob Marley");             //Create a song
         Song track2 = new Song("Three Little Birds", "Bob Marley");           //Create another song
         Song track3 = new Song("Is This Love", "Bob Marley");
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Beatles
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Beatles 
         this.albumTracks.add(track3);
         return albumTracks;                                                    //Return the songs for the Beatles in the form of an ArrayList
    }
}
