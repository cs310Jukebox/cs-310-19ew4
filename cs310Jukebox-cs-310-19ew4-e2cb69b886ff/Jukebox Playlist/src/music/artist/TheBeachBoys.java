package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class TheBeachBoys {
	
	ArrayList<Song> albumTracks;
	String albumTitles;
	
	public TheBeachBoys() {
	}
	
	public ArrayList<Song> getTheBeachBoysSongs() {
		
		albumTracks = new ArrayList<Song>();								//Instantiate the album so we can populate it below
		Song track1 = new Song("Surfin' U.S.A.", "The Beach Boys");			//Create a song
		Song track2 = new Song("Good Vibrations", "The Beach Boys");		//Create another song
		Song track3 = new Song("Kokomo", "The Beach Boys");					//Create another song
		this.albumTracks.add(track1);										//Add the first song to the song list for The Beach Boys
		this.albumTracks.add(track2);										//Add the second song to the song list for The Beach Boys
		this.albumTracks.add(track3);										//Add the third song to the song list for The Beach Boys
		return albumTracks;													//Return the songs for The Beach Boys in the form of an ArrayList
	}

}
