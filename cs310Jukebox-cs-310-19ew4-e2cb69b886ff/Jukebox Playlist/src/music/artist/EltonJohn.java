package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class EltonJohn {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public EltonJohn() {
    }
    
    public ArrayList<Song> getEltonJohnSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Candle in the Wind", "Elton John");             //Create a song
         Song track2 = new Song("Tiny Dancer", "Elton John");       			  //Create another song
         Song track3 = new Song("Rocket Man", "Elton John")						//Create third song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Elton John
         this.albumTracks.add(track2);                                          //Add the second song to song list for Elton John
         this.albumTracks.add(track3);											//Add the third song to song list for Elton John
         return albumTracks;                                                    //Return the songs for Elton John in the form of an ArrayList
    }
}