package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class MaydayParade {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public MaydayParade() {
    }
    
    public ArrayList<Song> getMaydayParadeSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Jamie All Over", "Mayday Parade");             //Create a song
         Song track2 = new Song("Miserable At Best", "Mayday Parade");          //Create another song
         Song track3 = new Song("Terrible Things", "Mayday Parade");            //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Mayday Parade
         this.albumTracks.add(track2);                                          //Add the second song to song list for Mayday Parade
         this.albumTracks.add(track3);                                          //Add the third song to song list for Mayday Parade
         return albumTracks;                                                    //Return the songs for Mayday Parade in the form of an ArrayList
    }
}
