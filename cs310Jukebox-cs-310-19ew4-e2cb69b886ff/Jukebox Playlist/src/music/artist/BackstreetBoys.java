package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class BackstreetBoys {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BackstreetBoys() {
    }
    
    public ArrayList<Song> getBackstreetBoysSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("I Want It That Way", "Backstreet Boys");             //Create a song
         Song track2 = new Song("Quit Playing Games With My Heart", "Backstreet Boys");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the Backstreet Boys
         this.albumTracks.add(track2);                                          //Add the second song to song list for the Backstreet Boys 
         return albumTracks;                                                    //Return the songs for the Backstreet Boys in the form of an ArrayList
    }
}
