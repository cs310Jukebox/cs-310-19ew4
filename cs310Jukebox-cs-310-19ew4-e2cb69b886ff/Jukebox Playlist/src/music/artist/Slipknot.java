package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class Slipknot {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Slipknot() {
    }
    
    public ArrayList<Song> getSlipknotSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("The Devil in I", "Slipknot");                  //Create a song
         Song track2 = new Song("Duality", "Slipknot");                         //Create another song
         Song track3 = new Song("Killpop", "Slipknot");							//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Slipknot
         this.albumTracks.add(track2);                                          //Add the second song to song list for Slipknot
         this.albumTracks.add(track3);										    //Add the third song to song list for Slipknot 
         return albumTracks;                                                    //Return the songs for Slipknot in the form of an ArrayList
    }
}