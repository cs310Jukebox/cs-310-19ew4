package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class MarianasTrench {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public MarianasTrench() {
    }
    
    public ArrayList<Song> getMarianasTrenchSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Who Do You Love", "Marianas Trench");          //Create a song
         Song track2 = new Song("Glimmer", "Marianas Trench");                  //Create another song
         Song track3 = new Song("Rhythm of Your Heart", "Marianas Trench");     //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Marianas Trench
         this.albumTracks.add(track2);                                          //Add the second song to song list for Marianas Trench 
         this.albumTracks.add(track3);                                          //Add the third song to song list for Marianas Trench 
         return albumTracks;                                                    //Return the songs for Marianas Trench in the form of an ArrayList
    }
}
