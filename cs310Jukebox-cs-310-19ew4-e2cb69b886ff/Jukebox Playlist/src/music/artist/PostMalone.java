package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class PostMalone {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    //Add Post Malone to the music artist list
    public PostMalone() {
    }
    
    public ArrayList<Song> getPostMaloneSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Psycho", "Post Malone");                       //Create a song
         Song track2 = new Song("Rockstar", "Post Malone");                     //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Post Malone
         this.albumTracks.add(track2);                                          //Add the second song to song list for Post Malone
         return albumTracks;                                                    //Return the songs for Post Malone in the form of an ArrayList
    }
}
