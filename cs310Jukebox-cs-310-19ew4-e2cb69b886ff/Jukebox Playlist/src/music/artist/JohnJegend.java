package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class JohnLegend {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public JohnLegend() {
    }
    
    public ArrayList<Song> getJohnLegendSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Everybody Knows", "John Legend");             //Create a song
         Song track2 = new Song("All of Me", "John Legend");        			 //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for John Legend
         this.albumTracks.add(track2);                                          //Add the second song to song list for John Legend 
         return albumTracks;                                                    //Return the songs for John Legend in the form of an ArrayList
    }
}