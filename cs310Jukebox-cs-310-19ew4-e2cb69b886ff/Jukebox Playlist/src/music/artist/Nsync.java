package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Nsync {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    //Add Nsync to the music artist list
    public Nsync() {
    }
    
    public ArrayList<Song> getNsyncSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Bye Bye Bye", "No Strings Attached");             //Create a song
         Song track2 = new Song("This I Promise You", "No Strings Attached");         //Create another song
         Song track3 = new Song ("Tearin' Up My Heart", "'N Sync");					// Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Nsync
         this.albumTracks.add(track2);                                          //Add the second song to song list for Nsync
         this.albumTracks.add(track3);											//Add the third song to song list for Nsync
         return albumTracks;                                                    //Return the songs for Nsync in the form of an ArrayList
    }
}


