//importing packages
package snhu.student.playlists;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

//logic to implement playlist
public class RichardBane_Playlist {
    public LinkedList<PlayableSong> StudentPlaylist(){
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	
	//adding songs from John Legend
	ArrayList<Song> johnLegendTracks = new ArrayList<Song>();
    JohnLegend johnLegendBand = new JohnLegend();
	johnLegendTracks = johnLegendBand.getJohnLegendSongs();
	
	playlist.add(johnLegendTracks.get(0)); //Everybody Knows
	playlist.add(johnLegendTracks.get(1)); //All of Me
	
	//adding songs from Elton John
	ArrayList<Song> eltonJohnTracks = new ArrayList<Song>();
	EltonJohn eltonJohnBand = new EltonJohn();
    eltonJohnTracks = eltonJohnBand.getEltonJohnSongs();
	
	playlist.add(eltonJohnTracks.get(0)); //Candle in the Wind
	playlist.add(eltonJohnTracks.get(1)); //Tiny Dancer
	playlist.add(eltonJohnTracks.get(2)); //Rocket Man
	
	//adding songs from Adele
    Adele adeleBand = new Adele();
	ArrayList<Song> adeleTracks = new ArrayList<Song>();
	adeleTracks = adeleBand.getAdeleSongs();
	
	playlist.add(adeleTracks.get(0)); //Hello
	playlist.add(adeleTracks.get(2)); //Rolling in the Deep
	
    return playlist;
	}
}