package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class TylerShockman_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> maydayParadeTracks = new ArrayList<Song>();
    MaydayParade maydayParadeBand = new MaydayParade();
	
    maydayParadeTracks = maydayParadeBand.getMaydayParadeSongs();
	
	playlist.add(maydayParadeTracks.get(0));
	playlist.add(maydayParadeTracks.get(1));
	playlist.add(maydayParadeTracks.get(2));
	
	
    MarianasTrench marianasTrenchBand = new MarianasTrench();
	ArrayList<Song> marianasTrenchTracks = new ArrayList<Song>();
    marianasTrenchTracks = marianasTrenchBand.getMarianasTrenchSongs();
	
	playlist.add(marianasTrenchTracks.get(0));
	playlist.add(marianasTrenchTracks.get(1));
	playlist.add(marianasTrenchTracks.get(2));

    Slipknot slipknotBand = new Slipknot();
	ArrayList<Song> slipknotTracks = new ArrayList<Song>();
    slipknotTracks = slipknotBand.getSlipknotSongs();
	
	playlist.add(slipknotTracks.get(0));
	
    return playlist;
	}
}
