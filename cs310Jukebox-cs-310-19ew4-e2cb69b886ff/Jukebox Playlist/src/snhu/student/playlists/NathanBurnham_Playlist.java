package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

// Creating playlist for Nathan Burnham
public class NathanBurnham_Playlist {
	
	public LinkedList<PlayableSong> StudentPlaylist() {
		
		// Adding songs from AntiFlag
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> antiflagTracks = new ArrayList<Song>();
		AntiFlag antiflagBand = new AntiFlag();
		
		antiflagTracks = antiflagBand.getAntiFlagSongs();
		
		playlist.add(antiflagTracks.get(0));
		playlist.add(antiflagTracks.get(1));
		playlist.add(antiflagTracks.get(2));
		
		// Add songs from Frank Zappa
		FrankZappa frankZappaBand = new FrankZappa();
		ArrayList<Song> frankZappaTracks = new ArrayList<Song>();
		frankZappaTracks = frankZappaBand.getFrankZappaSongs();
		
		playlist.add(frankZappaTracks.get(0));
		playlist.add(frankZappaTracks.get(1));
		
		// Add songs from Queen
		Queen queenBand = new Queen();
		ArrayList<Song> queenTracks = new ArrayList<Song>();
		queenTracks = queenBand.getQueenSongs();
		
		playlist.add(queenTracks.get(1));
		
		return playlist;
	}
}
