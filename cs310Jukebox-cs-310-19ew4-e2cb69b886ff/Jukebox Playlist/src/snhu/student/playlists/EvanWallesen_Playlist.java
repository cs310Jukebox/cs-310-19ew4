package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

//Creating Evan's playlist
public class EvanWallesen_Playlist {

	public LinkedList<PlayableSong> StudentPlaylist() {
		
		//Adding songs from The Beach Boys
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> theBeachBoysTracks = new ArrayList<Song>();
		TheBeachBoys theBeachBoysBand = new TheBeachBoys();
		
		theBeachBoysTracks = theBeachBoysBand.getTheBeachBoysSongs();
		
		playlist.add(theBeachBoysTracks.get(0)); // Surfin' U.S.A.
		playlist.add(theBeachBoysTracks.get(1)); // Good Vibrations
		playlist.add(theBeachBoysTracks.get(2)); // Kokomo
		
		//Adding songs from ABBA
		ABBA ABBAband = new ABBA();
		ArrayList<Song> ABBAtracks = new ArrayList<Song>();
		ABBAtracks = ABBAband.getABBASongs();
		
		playlist.add(ABBAtracks.get(0)); // Super Trouper
		playlist.add(ABBAtracks.get(1)); // Waterloo
		playlist.add(ABBAtracks.get(2)); // Take a Chance On Me
		
		//Adding songs from Queen
		Queen queenBand = new Queen();
		ArrayList<Song> queenTracks = new ArrayList<Song>();
		queenTracks = queenBand.getQueenSongs();
		
		playlist.add(queenTracks.get(0)); // Bohemian Rhapsody
		playlist.add(queenTracks.get(2)); // Another One Bites The Dust
		
		return playlist;
	}
}
