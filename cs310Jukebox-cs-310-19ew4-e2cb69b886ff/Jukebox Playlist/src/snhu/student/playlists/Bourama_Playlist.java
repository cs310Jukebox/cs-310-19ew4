/**
 * Class: CS-310
 * Southern New Hampshire Universality
 * Date : 2019-04-07
 */
package snhu.student.playlists;

import java.util.ArrayList;
import java.util.LinkedList;
import music.artist.*;
import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
/**
 * This class create and populate my playlist by add track for Salif keita and Bob Marley
 * @author Bourama Mangara
 * @version 1.0
 *
 */
public class Bourama_Playlist {
	public LinkedList<PlayableSong> StudentPlaylist(){
		
		LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
		ArrayList<Song> bMarleyTracks = new ArrayList<Song>();
	    BobMarley bMarley = new BobMarley();
		
	    bMarleyTracks = bMarley.getSongs();
		
		playlist.add(bMarleyTracks.get(0));
		playlist.add(bMarleyTracks.get(1));
		playlist.add(bMarleyTracks.get(2));
		
		
	    SalifKeita sKeita= new SalifKeita();
		ArrayList<Song> sKeitaTracks = new ArrayList<Song>();
	    sKeitaTracks = sKeita.getSongs();
		
		playlist.add(sKeitaTracks.get(0));
		playlist.add(sKeitaTracks.get(1));
		//Added Artist/band from a team mate
		   // The Beattles tracks
		TheBeatles theBeatles = new TheBeatles();
		ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		beatlesTracks = theBeatles.getBeatlesSongs();
			
		playlist.add(beatlesTracks.get(0));
		playlist.add(beatlesTracks.get(1));
		
		 //Back Street Boys tracks
		BackstreetBoys backStreetBoys = new BackstreetBoys();
		ArrayList<Song> backStreetBoysracks = new ArrayList<Song>();
		backStreetBoysracks = backStreetBoys.getBackstreetBoysSongs();
			
		playlist.add(backStreetBoysracks.get(0));
		playlist.add(backStreetBoysracks.get(1));
		
		 // Creed tracks
		Creed creed = new Creed();
		ArrayList<Song> creedracks = new ArrayList<Song>();
		creedracks = creed.getCreedSongs();
			
		playlist.add(creedracks.get(0));
		playlist.add(creedracks.get(1));
	    return playlist;
		}
}
